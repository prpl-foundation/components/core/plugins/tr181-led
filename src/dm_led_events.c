/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "led.h"
#include "dm_led.h"

void _led_start_cycle(UNUSED const char* const event_name,
                      const amxc_var_t* const event_data,
                      UNUSED void* const priv) {
    amxd_object_t* led_instance = amxd_dm_signal_get_object(get_dm(), event_data);
    amxd_object_t* current_element = amxd_object_get_child(led_instance, "CurrentCycleElement");
    led_instance_t* priv_data = (led_instance_t*) led_instance->priv;

    if(priv_data->cycle_timer != NULL) {
        amxd_trans_t trans;

        SAH_TRACEZ_INFO(ME, "Starting LED cycle period");
        amxd_trans_init(&trans);
        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_select_object(&trans, current_element);
        amxd_trans_set_value(cstring_t, &trans, "CycleElementReference", "");
        if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to update CycleElementReference");
        }
        if((amxp_timer_get_state(priv_data->cycle_timer) != amxp_timer_started)
           && (amxp_timer_get_state(priv_data->cycle_timer) != amxp_timer_running)) {
            amxp_timer_start(priv_data->cycle_timer, 0);
        }

        amxd_trans_clean(&trans);
    }
}

void _led_stop_cycle(UNUSED const char* const event_name,
                     const amxc_var_t* const event_data,
                     UNUSED void* const priv) {
    amxd_object_t* led_instance = amxd_dm_signal_get_object(get_dm(), event_data);
    led_instance_t* priv_data = (led_instance_t*) led_instance->priv;

    SAH_TRACEZ_INFO(ME, "Stopping LED cycle period");
    if(priv_data->cycle_timer != NULL) {
        amxp_timer_stop(priv_data->cycle_timer);
    }
    led_set_brightness(priv_data, 0, true);
}

void _led_app_start(UNUSED const char* const sig_name,
                    UNUSED const amxc_var_t* const data,
                    UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "TR181-LED pluging starting");
    led_init_all();
}

static void cycle_element_change_brightness(cycle_element_t* celem_info, amxc_var_t* var, const amxc_var_t* const event_data) {
    int32_t new_brightness = GET_INT32(var, "to");

    if(celem_info->brightness != new_brightness) {
        amxd_object_t* led_instance = amxd_object_get_parent(amxd_object_get_parent(get_object(GET_CHAR(event_data, "path"))));
        led_instance_t* info_led = (led_instance_t*) led_instance->priv;

        celem_info->brightness = new_brightness;
        if((info_led == NULL) || (info_led->current_element == NULL)) {
            return;
        }
        if(&celem_info->ll_it == info_led->current_element) {
            led_set_brightness(info_led, new_brightness, false);
        }
    }
}

void _cycle_element_changed(UNUSED const char* const event_name,
                            const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    amxd_object_t* celement_instance = amxd_dm_signal_get_object(get_dm(), event_data);
    amxd_object_t* led_instance = amxd_object_get_parent(amxd_object_get_parent(get_object(GET_CHAR(event_data, "path"))));
    led_instance_t* info_led = (led_instance_t*) led_instance->priv;
    cycle_element_t* celem_info = (cycle_element_t*) celement_instance->priv;
    amxc_llist_t* llist_cycle_period = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* var = NULL;
    amxc_string_t path;
    amxc_string_init(&path, 0);

    when_null(celem_info, exit);

    llist_cycle_period = celem_info->ll_it.llist;
    var = GET_ARG(params, "Duration");
    if(var != NULL) {
        celem_info->duration_ms = GET_UINT32(var, "to");
    }
    var = GET_ARG(params, "FadeInterval");
    if(var != NULL) {
        celem_info->fade_interval_ms = GET_UINT32(var, "to");
    }
    var = GET_ARG(params, "Enable");
    if(var != NULL) {
        celem_info->enabled = GET_BOOL(var, "to");
    }
    var = GET_ARG(params, "Color");
    if(var != NULL) {
        hex_to_rgb(GET_CHAR(var, "to"), &celem_info->color);
    }
    amxc_string_setf(&path, "%sBrightness", get_prefix());
    var = GET_ARG(params, amxc_string_get(&path, 0));
    if(var != NULL) {
        cycle_element_change_brightness(celem_info, var, event_data);
    }
    var = GET_ARG(params, "Order");
    if(var != NULL) {
        amxc_llist_it_take(&celem_info->ll_it);
        amxc_llist_set_at(llist_cycle_period,
                          GET_UINT32(var, "to") - 1,
                          &celem_info->ll_it);
    }
    if((info_led != NULL) && (info_led->cycle_timer != NULL)) {
        amxp_timer_start(info_led->cycle_timer, 0);
    }
exit:
    amxc_string_clean(&path);
    return;
}

void _cycle_element_added(UNUSED const char* const event_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    amxd_object_t* celements = amxd_dm_signal_get_object(get_dm(), event_data);
    amxd_object_t* celement_instance = amxd_object_get_instance(celements, NULL, GET_UINT32(event_data, "index"));
    amxd_object_t* led_instance = amxd_object_get_parent(get_object(GET_CHAR(event_data, "path")));
    cycle_element_t* celem_info = NULL;
    led_instance_t* info_led = (led_instance_t*) led_instance->priv;

    if((info_led != NULL) && (info_led->llist_cycle_period != NULL)) {
        celem_info = (cycle_element_t*) calloc(1, sizeof(cycle_element_t));
        element_init_info(celem_info, celement_instance);
        if(amxc_llist_set_at(info_led->llist_cycle_period,
                             amxd_object_get_uint32_t(celement_instance, "Order", NULL) - 1,
                             &celem_info->ll_it)) {
            element_clean(&celem_info->ll_it);
        }
        if((amxp_timer_get_state(info_led->cycle_timer) != amxp_timer_started)
           && (amxp_timer_get_state(info_led->cycle_timer) != amxp_timer_running)) {
            amxp_timer_start(info_led->cycle_timer, 0);
        }
    }
}

void _cycle_element_removed(UNUSED const char* const event_name,
                            const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    amxd_object_t* celements = amxd_dm_signal_get_object(get_dm(), event_data);
    amxd_object_t* led_instance = amxd_object_get_parent(celements);
    led_instance_t* led_info = (led_instance_t*) led_instance->priv;

    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_llist_it_t* element_it = amxc_llist_get_at(led_info->llist_cycle_period, GET_UINT32(params, "Order") - 1);
    if(element_it == led_info->current_element) {
        amxp_timer_stop(led_info->cycle_timer);
        led_info->current_element = NULL;
        amxp_timer_start(led_info->cycle_timer, 0);
    }
    if(element_it != NULL) {
        element_clean(element_it);
    }
}

void _led_suspend(UNUSED const char* const event_name,
                  UNUSED const amxc_var_t* const event_data,
                  UNUSED void* const priv) {
    amxd_object_t* led_instance = NULL;
    led_instance_t* priv_data = NULL;
    cycle_element_t* current_element = NULL;

    amxd_object_for_each(instance, it, get_object(LED_TR181_PATH)) {
        led_instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        priv_data = (led_instance_t*) led_instance->priv;
        if(priv_data != NULL) {
            priv_data->suspend = !priv_data->suspend;
            if(priv_data->current_element != NULL) {
                current_element = amxc_llist_it_get_data(priv_data->current_element, cycle_element_t, ll_it);
                led_set_color(priv_data, current_element->color);
            }
        }
    }
}