/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "led.h"
#include "dm_led.h"

static bool led_has_celement_enabled(led_instance_t* led_info) {
    cycle_element_t* cycle_info = NULL;

    when_null(led_info, exit);
    amxc_llist_for_each(it, led_info->llist_cycle_period) {
        cycle_info = amxc_llist_it_get_data(it, cycle_element_t, ll_it);
        if((cycle_info != NULL) && cycle_info->enabled) {
            return true;
        }
    }
exit:
    return false;
}

static bool led_valid_state(amxd_object_t* led_instance) {
    bool retval = false;
    amxc_var_t led_params;

    amxc_var_init(&led_params);

    when_null(led_instance, exit);
    amxd_object_get_params(led_instance, &led_params, amxd_dm_access_protected);

    retval = strcmp("InUse", GET_CHAR(&led_params, "Status")) == 0 &&
        GET_INT32(&led_params, "CyclePeriodRepetitions") != 0 &&
        GET_INT32(&led_params, "CycleElementNumberOfEntries") != 0 &&
        led_has_celement_enabled((led_instance_t*) led_instance->priv);
exit:
    amxc_var_clean(&led_params);
    return retval;
}

static bool loop_next_element(led_instance_t* led_instance, cycle_element_t** next_element) {
    amxc_llist_it_t* current_element_it = NULL;
    amxc_llist_it_t* next_element_it = NULL;
    bool looped = false;
    cycle_element_t* valid_element = NULL;

    when_null(led_instance, exit);

    current_element_it = led_instance->current_element;
    if((current_element_it == NULL) || (amxc_llist_it_get_next(current_element_it) == NULL)) {
        next_element_it = amxc_llist_get_first(led_instance->llist_cycle_period);
        looped = true;
    } else {
        next_element_it = amxc_llist_it_get_next(current_element_it);
    }
    when_null(next_element_it, exit);
    valid_element = amxc_llist_it_get_data(next_element_it, cycle_element_t, ll_it);

    while(!valid_element->enabled && next_element_it != current_element_it) {
        next_element_it = amxc_llist_it_get_next(next_element_it);
        if(next_element_it == NULL) {
            next_element_it = amxc_llist_get_first(led_instance->llist_cycle_period);
            looped = true;
        }
        valid_element = amxc_llist_it_get_data(next_element_it, cycle_element_t, ll_it);
    }
exit:
    *next_element = valid_element;
    return looped;
}

void element_init_info(cycle_element_t* cycle_info, amxd_object_t* celement_instance) {
    amxc_var_t params;
    amxc_string_t path;
    amxc_string_init(&path, 0);
    amxc_var_init(&params);
    amxc_string_setf(&path, "%sBrightness", get_prefix());

    when_null(cycle_info, exit);
    when_null(celement_instance, exit);

    amxd_object_get_params(celement_instance, &params, amxd_dm_access_protected);
    cycle_info->fade_interval_ms = GET_UINT32(&params, "FadeInterval");
    cycle_info->duration_ms = GET_INT32(&params, "Duration");
    cycle_info->enabled = GET_BOOL(&params, "Enable");
    cycle_info->obj_path = amxd_object_get_path(celement_instance, AMXD_OBJECT_INDEXED);
    hex_to_rgb(GET_CHAR(&params, "Color"), &cycle_info->color);
    cycle_info->brightness = GET_INT32(&params, amxc_string_get(&path, 0));
    celement_instance->priv = cycle_info;
exit:
    amxc_string_clean(&path);
    amxc_var_clean(&params);
}

static void update_cycle_count(amxd_object_t* led_instance) {
    amxd_status_t status = amxd_status_unknown_error;
    int32_t current_val = amxd_object_get_int32_t(led_instance, "CyclePeriodRepetitions", &status);
    if((status == amxd_status_ok) && (current_val > 0)) {
        // Do not use a transaction here, that will trigger the led_start_cycle/led_stop_cycle functions
        amxd_object_set_int32_t(led_instance, "CyclePeriodRepetitions", current_val - 1);
    }
}

static void restart_timer_element(cycle_element_t* next_element, amxd_object_t* led_instance, bool multiple_cycle_elements, amxp_timer_t* timer) {
    amxd_status_t status = amxd_status_unknown_error;
    int32_t current_val = amxd_object_get_int32_t(led_instance, "CyclePeriodRepetitions", &status);

    if(((status == amxd_status_ok) && (current_val > -1)) || (multiple_cycle_elements)) {
        amxp_timer_start(timer, next_element->duration_ms);
    }
}

void element_start_next(amxp_timer_t* timer, void* priv) {
    amxd_object_t* current_element_obj = NULL;
    cycle_element_t* next_element = NULL;
    amxd_object_t* led_instance = (amxd_object_t*) priv;
    led_instance_t* led_info = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null(led_instance, exit);
    led_info = (led_instance_t*) led_instance->priv;
    when_null(led_info, exit);

    if(led_valid_state(led_instance)) {
        cycle_element_t* current_element = led_info->current_element == NULL ? NULL : amxc_llist_it_get_data(led_info->current_element, cycle_element_t, ll_it);
        if(loop_next_element(led_info, &next_element) &&
           (led_info->current_element != NULL)) {
            update_cycle_count(led_instance);
        }
        when_null(next_element, exit);

        current_element_obj = amxd_object_get_child(led_instance, "CurrentCycleElement");
        amxd_trans_select_object(&trans, current_element_obj);
        amxd_trans_set_value(cstring_t, &trans, "CycleElementReference", next_element->obj_path);
        if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to update CycleElementReference");
        }

        led_set_element(led_info, next_element);
        restart_timer_element(next_element, led_instance, current_element != next_element, timer);
    } else {
        SAH_TRACEZ_NOTICE(ME, "LED device in invalid state, stopping Cycle period");
        led_set_brightness(led_info, 0, true);
        amxp_timer_stop(timer);
    }

exit:
    amxd_trans_clean(&trans);
    return;
}
