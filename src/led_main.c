/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "dm_led.h"
#include "led.h"

typedef struct {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    char* default_led_path;
} app_t;

static app_t app;

amxd_dm_t* get_dm(void) {
    return app.dm;
}

amxo_parser_t* get_parser(void) {
    return app.parser;
}

amxc_var_t* get_config(void) {
    amxo_parser_t* parser = get_parser();
    return parser != NULL ? &(parser->config) : NULL;
}

amxd_object_t* get_object(const char* path) {
    amxd_object_t* object = NULL;

    when_str_empty(path, exit);
    object = amxd_dm_findf(get_dm(), "%s", path);
exit:
    return object;
}

char* get_default_path(void) {
    return app.default_led_path;
}

const char* get_prefix(void) {
    amxc_var_t* setting = amxo_parser_get_config(get_parser(), "prefix_");
    return amxc_var_constcast(cstring_t, setting);
}

void led_device_clean(amxc_llist_it_t* it) {
    led_device_t* uled = NULL;

    if(it != NULL) {
        uled = amxc_container_of(it, led_device_t, ll_it);
        amxc_llist_it_take(it);
        free(uled);
        uled = NULL;
    }
}

void element_clean(amxc_llist_it_t* it) {
    cycle_element_t* cycle_info = NULL;

    if(it != NULL) {
        cycle_info = amxc_container_of(it, cycle_element_t, ll_it);
        amxc_llist_it_take(it);
        free(cycle_info->obj_path);
        free(cycle_info);
        cycle_info = NULL;
    }
}

static int deinit_led(amxd_object_t* led_instance) {
    int retval = 1;
    led_instance_t* led_info = NULL;

    when_null(led_instance, exit);
    led_info = led_instance->priv;
    if(led_info != NULL) {
        amxp_timer_delete(&led_info->cycle_timer);
        led_set_brightness(led_info, 0, true);
        amxc_llist_delete(&led_info->llist_led_device, led_device_clean);
        amxc_llist_delete(&led_info->llist_cycle_period, element_clean);
        free(led_info);
        led_info = NULL;
        retval = 0;
    }
exit:
    return retval;
}

int led_deinit_all(void) {
    int retval = 0;
    SAH_TRACEZ_INFO(ME, "Stopping LED devices");
    amxd_object_for_each(instance, it, get_object(LED_TR181_PATH)) {
        retval |= deinit_led(amxc_llist_it_get_data(it, amxd_object_t, it));
    }
    return retval;
}

static int take_led(amxd_object_t* uled_instance) {
    int retval = 1;
    led_instance_t* uled_info = NULL;

    when_failed(led_init(uled_instance), exit);
    uled_info = uled_instance->priv;
    when_null(uled_info, exit);
    when_true(uled_info->controlled, exit);
    uled_info->controlled = true;
    retval = 0;
exit:
    return retval;
}

static amxc_llist_t* get_devices_led(amxd_object_t* led_instance) {
    led_instance_t* uled_info = NULL;

    if(led_init(led_instance) == 0) {
        uled_info = led_instance->priv;
        return uled_info->llist_led_device;
    }
    return NULL;
}

static led_device_t* get_device_info(const char* device_name, const char* colorhex) {
    led_device_t* uled_device_info = NULL;

    when_false(led_device_exists(device_name), exit);

    uled_device_info = calloc(1, sizeof(led_device_t));
    when_null(uled_device_info, exit);

    uled_device_info->sysfs_name = device_name;
    hex_to_rgb(colorhex, &(uled_device_info->color));
    uled_device_info->max_brightness = led_brightness(device_name, true);
    uled_device_info->has_fade = led_validate_trigger(device_name, "timer", "mode", "fade");
exit:
    return uled_device_info;
}

static void update_device_max_brightness(led_device_t* uled_device_info, amxd_object_t* ref_led_instance) {
    amxc_string_t path;
    amxd_status_t status = amxd_status_unknown_error;
    int32_t current_val = -1;
    const char* path_str = NULL;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "%sMaxBrightness", get_prefix());
    path_str = amxc_string_get(&path, 0);

    current_val = amxd_object_get_int32_t(ref_led_instance, path_str, &status);
    when_failed_trace(status, exit, ERROR, "Failed to get %s", path_str);

    if(current_val < 0) {
        amxd_trans_t trans;

        amxd_trans_init(&trans);

        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_select_object(&trans, ref_led_instance);
        amxd_trans_set_value(int32_t, &trans, path_str, uled_device_info->max_brightness);
        if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to update %s", path_str);
        }

        amxd_trans_clean(&trans);
    } else {
        uled_device_info->max_brightness = current_val;
    }

exit:
    amxc_string_clean(&path);
}

static int init_uled(led_instance_t* parent_led_info, amxd_object_t* uled_instance) {
    int retval = 1;
    led_device_t* uled_device_info = NULL;
    amxc_llist_t* list_devices_uled = NULL;
    amxd_object_t* ref_led_instance = get_object(GET_CHAR(amxd_object_get_param_value(uled_instance, "LEDReference"), NULL));

    when_null(ref_led_instance, exit);
    when_failed(take_led(ref_led_instance), exit);

    list_devices_uled = get_devices_led(ref_led_instance);

    if((list_devices_uled != NULL) && (amxc_llist_size(list_devices_uled) != 0)) {
        retval = amxc_llist_move(parent_led_info->llist_led_device, list_devices_uled);
    } else {
        uled_device_info = get_device_info(GET_CHAR(amxd_object_get_param_value(ref_led_instance, "Name"), NULL),
                                           GET_CHAR(amxd_object_get_param_value(uled_instance, "SupportedColor"), NULL));
        when_null(uled_device_info, exit);
        update_device_max_brightness(uled_device_info, ref_led_instance);
        retval = amxc_llist_append(parent_led_info->llist_led_device, &(uled_device_info->ll_it));
    }
exit:
    return retval;
}

static int init_uleds(amxd_object_t* led_instance) {
    amxd_object_t* uled_instance = NULL;
    led_instance_t* led_info = NULL;
    amxd_object_t* uleds = NULL;
    int retval = 0;

    fail_when_null(led_instance);
    fail_when_null(led_instance->priv);

    led_info = led_instance->priv;
    uleds = amxd_object_get_child(led_instance, "ULED");
    amxd_object_for_each(instance, it, uleds) {
        uled_instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        retval |= init_uled(led_info, uled_instance);
    }
    return retval;
}

static int init_cycle_elements(amxd_object_t* led_instance) {
    amxd_object_t* celement_instance = NULL;
    amxd_object_t* celements = NULL;
    cycle_element_t* celem_info = NULL;
    led_instance_t* priv_data = NULL;
    int retval = 1;

    when_null(led_instance, exit);
    celements = amxd_object_get_child(led_instance, "CycleElement");
    priv_data = (led_instance_t*) led_instance->priv;
    when_null(celements, exit);
    when_null(priv_data, exit);

    amxd_object_for_each(instance, it, celements) {
        celement_instance = amxc_container_of(it, amxd_object_t, it);
        when_null(celement_instance, exit);
        celem_info = (cycle_element_t*) calloc(1, sizeof(cycle_element_t));
        when_null(celem_info, exit);
        element_init_info(celem_info, celement_instance);
        if(amxc_llist_set_at(priv_data->llist_cycle_period,
                             amxd_object_get_uint32_t(celement_instance, "Order", NULL) - 1,
                             &celem_info->ll_it)) {
            element_clean(&celem_info->ll_it);
            goto exit;
        }
    }
    retval = 0;
exit:
    return retval;
}

int led_init(amxd_object_t* led_instance) {
    led_instance_t* priv_data = NULL;
    int retval = 1;

    if(led_instance->priv == NULL) {
        priv_data = calloc(1, sizeof(led_instance_t));
        when_null(priv_data, exit);
        led_instance->priv = priv_data;

        amxc_llist_new(&priv_data->llist_led_device);
        when_failed(init_uleds(led_instance), exit);

        amxc_llist_new(&priv_data->llist_cycle_period);
        when_failed(init_cycle_elements(led_instance), exit);

        when_failed(amxp_timer_new(&(priv_data->cycle_timer), element_start_next, led_instance), exit);
        retval = amxp_timer_start(priv_data->cycle_timer, 0);

        priv_data->suspend = false;
    } else {
        priv_data = (led_instance_t*) led_instance->priv;
        retval = priv_data->llist_cycle_period == NULL;
    }

exit:
    return retval;
}

int led_init_all(void) {
    int retval = 0;

    SAH_TRACEZ_INFO(ME, "Initializing LED devices and loading extension module");

    when_failed_trace(led_open_sysfsctrl(), exit, ERROR, "Could not load extension module");

    amxd_object_for_each(instance, it, get_object(LED_TR181_PATH)) {
        retval |= led_init(amxc_llist_it_get_data(it, amxd_object_t, it));
    }
exit:
    return retval;
}

static int init(amxd_dm_t* dm, amxo_parser_t* parser) {
    app.dm = dm;
    app.parser = parser;
    amxc_var_t* sysfs_path = amxo_parser_get_config(parser, "led_devices_path");
    app.default_led_path = amxc_var_dyncast(cstring_t, sysfs_path);
    return 0;
}

static int cleanup(void) {
    int retval = led_deinit_all();
    free(app.default_led_path);
    app.default_led_path = NULL;
    app.dm = NULL;
    app.parser = NULL;
    return retval;
}

int _led_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = 0;

    switch(reason) {
    case AMXO_START:
        SAH_TRACEZ_NOTICE(ME, "Starting tr181-led plugin");
        retval = init(dm, parser);
        break;
    case AMXO_STOP:
        SAH_TRACEZ_NOTICE(ME, "Stopping tr181-led plugin");
        retval = cleanup();
        break;
    }
    return retval;
}
