/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "led.h"
#include "dm_led.h"

void hex_to_rgb(const char* hex, rgb_t* rgb_representation) {
    if((hex != NULL) &&
       (rgb_representation != NULL) &&
       (strlen(hex) == 6)) {
        sscanf(hex, "%02hhx%02hhx%02hhx", &(rgb_representation->red)
               , &(rgb_representation->green)
               , &(rgb_representation->blue));
    }
}

bool led_device_exists(const char* device_name) {
    bool retval = false;
    char path[MAX_LEN_PATH];
    struct stat s_dev = {0};

    when_str_empty(device_name, exit);
    when_not_null(strchr(device_name, '/'), exit);
    snprintf(path, MAX_LEN_PATH, "%s/%s", get_default_path(), device_name);
    if(stat(path, &s_dev) == 0) {
        retval = true;
    }
exit:
    return retval;
}

static bool led_sysfs_write(const char* led_name, const char* led_attribute_name, const char* led_attribute_value) {
    bool retval = false;
    amxo_parser_t* parser = get_parser();
    const char* const so_name = GETP_CHAR(&parser->config, "so-name");
    const char* const mod_name = GETP_CHAR(&parser->config, "mod-name");
    amxc_var_t data;
    amxc_var_t ret;
    amxc_string_t sysfs_path;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_string_init(&sysfs_path, 0);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_string_setf(&sysfs_path, "%s", get_default_path());
    amxc_var_add_key(cstring_t, &data, "sysfs_path", amxc_string_get(&sysfs_path, 0));
    amxc_var_add_key(cstring_t, &data, "led_name", led_name);
    amxc_var_add_key(cstring_t, &data, "led_attribute_name", led_attribute_name);
    amxc_var_add_key(cstring_t, &data, "led_attribute_value", led_attribute_value);

    when_failed_trace(amxm_execute_function(so_name, mod_name, "led-sysfs-write", &data, &ret) != 0, exit, ERROR, "Call to led_sysfs_write failed");

    retval = amxc_var_constcast(bool, &ret);

exit:
    amxc_string_clean(&sysfs_path);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);

    return retval;
}

static bool led_sysfs_write_uint32(const char* led_name, const char* led_attribute_name, uint32_t led_attribute_value) {
    bool retval = false;
    char buff[MAX_LEN_BUFFER] = {0};

    snprintf(buff, sizeof(buff), "%u", led_attribute_value);
    retval = led_sysfs_write(led_name, led_attribute_name, buff);

    return retval;
}

bool led_validate_trigger(const char* device_name, const char* trigger, const char* parameter, const char* value) {
    bool retval = false;
    char path[MAX_LEN_PATH] = {0};
    struct stat info_param = {0};

    when_null(device_name, exit);
    when_null(trigger, exit);
    when_null(parameter, exit);

    when_false(led_sysfs_write(device_name, SYSFS_LED_TRIGGER, trigger), exit);
    snprintf(path, sizeof(path), "%s/%s/%s", get_default_path(), device_name, parameter);
    when_failed(stat(path, &info_param), exit);
    retval = (value == NULL) || (led_sysfs_write(device_name, parameter, value));
exit:
    led_sysfs_write(device_name, SYSFS_LED_TRIGGER, LED_DEFAULT_TRIGGER);
    return retval;
}

bool led_set_mode(const char* device_name, const char* trigger, const char* mode) {
    bool retval = led_sysfs_write(device_name, SYSFS_LED_TRIGGER, trigger);
    retval &= led_sysfs_write(device_name, SYSFS_LED_MODE, mode);
    return retval;
}

bool led_set_fade(led_instance_t* led_info, uint32_t fade_ms) {
    bool retval = false;
    led_device_t* device_info = NULL;

    when_null(led_info, exit);
    retval = true;
    amxc_llist_for_each(it, led_info->llist_led_device) {
        device_info = amxc_llist_it_get_data(it, led_device_t, ll_it);
        if(device_info->has_fade) {
            retval &= led_set_mode(device_info->sysfs_name, "timer", "fade");
            retval &= led_sysfs_write_uint32(device_info->sysfs_name, SYSFS_LED_DELAY_ON, fade_ms / 2);
            retval &= led_sysfs_write_uint32(device_info->sysfs_name, SYSFS_LED_DELAY_OFF, fade_ms / 2);
        }
    }
exit:
    return retval;
}

static bool led_validate_colors(led_instance_t* led_info, rgb_t color_info, rgb_led_t* color_led_ref) {
    led_device_t* device_info = NULL;
    bool valid_red = color_info.red == 0 ? true : false;
    bool valid_green = color_info.green == 0 ? true : false;
    bool valid_blue = color_info.blue == 0 ? true : false;
    bool single_led = false;
    rgb_t red_component = {color_info.red, 0, 0};
    rgb_t green_component = {0, color_info.green, 0};
    rgb_t blue_component = {0, 0, color_info.blue};

    // compare each component (r,g,b) with the associated color supported
    // if the component is 0 => skip
    amxc_llist_for_each(it, led_info->llist_led_device) {
        device_info = amxc_llist_it_get_data(it, led_device_t, ll_it);
        when_true_status(same_color(device_info->color, color_info), exit, single_led = true);

        if((valid_red == false) && same_color(device_info->color, red_component)) {
            valid_red = true;
            color_led_ref->red_led = device_info->sysfs_name;
        }
        if((valid_green == false) && same_color(device_info->color, green_component)) {
            valid_green = true;
            color_led_ref->green_led = device_info->sysfs_name;
        }
        if((valid_blue == false) && same_color(device_info->color, blue_component)) {
            valid_blue = true;
            color_led_ref->blue_led = device_info->sysfs_name;
        }
        if(valid_red && valid_green && valid_blue) {
            break;
        }
    }
exit:
    return (valid_red == true && valid_green == true && valid_blue == true) || single_led == true;
}

static bool led_apply_colors(led_instance_t* led_info, rgb_t color_info, rgb_led_t color_led_ref) {
    led_device_t* device_info = NULL;
    bool retval = false;
    // apply color component by component (assume that each led handles only one component)
    amxc_llist_for_each(it, led_info->llist_led_device) {
        device_info = amxc_llist_it_get_data(it, led_device_t, ll_it);
        if(same_color(device_info->color, color_info)) {
            // case of a single led handling the color
            retval &= led_sysfs_write_uint32(device_info->sysfs_name, SYSFS_LED_BRIGHTNESS, device_info->max_brightness);
            // turn off other leds
            it = amxc_llist_it_get_next(it);
            while(it != NULL) {
                device_info = amxc_llist_it_get_data(it, led_device_t, ll_it);
                retval &= led_sysfs_write_uint32(device_info->sysfs_name, SYSFS_LED_BRIGHTNESS, 0);
                it = amxc_llist_it_get_next(it);
            }
            break;
        }
        if(strcmp(color_led_ref.red_led, device_info->sysfs_name) == 0) {
            retval &= led_sysfs_write_uint32(device_info->sysfs_name, SYSFS_LED_BRIGHTNESS, device_info->max_brightness);
        } else if(strcmp(color_led_ref.green_led, device_info->sysfs_name) == 0) {
            retval &= led_sysfs_write_uint32(device_info->sysfs_name, SYSFS_LED_BRIGHTNESS, device_info->max_brightness);
        } else if(strcmp(color_led_ref.blue_led, device_info->sysfs_name) == 0) {
            retval &= led_sysfs_write_uint32(device_info->sysfs_name, SYSFS_LED_BRIGHTNESS, device_info->max_brightness);
        } else {
            retval &= led_sysfs_write_uint32(device_info->sysfs_name, SYSFS_LED_BRIGHTNESS, 0);
        }
    }
    return retval;
}

bool led_set_color(led_instance_t* led_info, rgb_t color_info) {
    bool retval = false;
    led_device_t* device_info = NULL;
    rgb_t color_off = {0, 0, 0};
    rgb_led_t color_led_ref = {"", "", ""};

    when_null(led_info, exit);
    retval = true;

    if((led_info->suspend == true) || same_color(color_off, color_info)) {
        amxc_llist_for_each(it, led_info->llist_led_device) {
            device_info = amxc_llist_it_get_data(it, led_device_t, ll_it);
            retval &= led_sysfs_write_uint32(device_info->sysfs_name, SYSFS_LED_BRIGHTNESS, 0);
        }
        goto exit;
    }

    // unsupported color (missing led or unsupported component)
    when_false_status(led_validate_colors(led_info, color_info, &color_led_ref), exit, retval = false);

    retval = led_apply_colors(led_info, color_info, color_led_ref);
exit:
    return retval;
}

static bool led_is_brightness_applied(led_instance_t* led_info) {
    bool uniform_brightness_capacity = true;
    led_device_t* device_info = NULL;

    if(amxc_llist_size(led_info->llist_led_device) > 1) {
        amxc_llist_it_t* first = amxc_llist_get_first(led_info->llist_led_device);
        device_info = amxc_llist_it_get_data(first, led_device_t, ll_it);
        int32_t ref_val = device_info->max_brightness;

        amxc_llist_for_each(it, led_info->llist_led_device) {
            device_info = amxc_llist_it_get_data(it, led_device_t, ll_it);
            if(device_info->max_brightness != ref_val) {
                uniform_brightness_capacity = false;
                break;
            }
        }
    }

    return uniform_brightness_capacity;
}

static void fill_led_info(amxc_var_t* data, amxc_llist_t* llist_led_device) {
    amxc_var_t* llist_dev = amxc_var_add_key(amxc_llist_t, data, "led_devices", NULL);
    amxc_llist_for_each(it, llist_led_device) {
        amxc_var_t led_data;
        amxc_var_init(&led_data);
        amxc_var_set_type(&led_data, AMXC_VAR_ID_HTABLE);
        led_device_t* device_info = amxc_llist_it_get_data(it, led_device_t, ll_it);
        amxc_var_add_key(cstring_t, &led_data, "sysfs_name", device_info->sysfs_name);
        amxc_var_add_key(int32_t, &led_data, "max_brightness", device_info->max_brightness);
        amxc_var_add(amxc_htable_t, llist_dev, amxc_var_constcast(amxc_htable_t, &led_data));
        amxc_var_clean(&led_data);
    }
}

bool led_set_brightness(led_instance_t* led_info, int32_t brightness_info, bool force) {
    bool retval = false;
    amxo_parser_t* parser = get_parser();
    const char* const so_name = GETP_CHAR(&parser->config, "so-name");
    const char* const mod_name = GETP_CHAR(&parser->config, "mod-name");

    when_null(led_info, exit);
    retval = true;
    if((force == false) &&
       ((brightness_info < 0) || (led_is_brightness_applied(led_info) == false))) {
        goto exit;
    } else {
        amxc_var_t data;
        amxc_var_t ret;
        amxc_string_t sysfs_path;

        amxc_var_init(&data);
        amxc_var_init(&ret);
        amxc_string_init(&sysfs_path, 0);

        amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
        amxc_string_setf(&sysfs_path, "%s", get_default_path());
        amxc_var_add_key(cstring_t, &data, "sysfs_path", amxc_string_get(&sysfs_path, 0));
        amxc_var_add_key(int32_t, &data, "brightness_info", brightness_info);
        amxc_var_add_key(bool, &data, "force", force);
        amxc_var_add_key(bool, &data, "led_suspend", led_info->suspend);
        fill_led_info(&data, led_info->llist_led_device);

        when_failed_trace(amxm_execute_function(so_name, mod_name, "led-set-brightness", &data, &ret) != 0, skip, ERROR, "Call to led_set_brightness failed");

        retval = amxc_var_constcast(bool, &ret);

skip:
        amxc_string_clean(&sysfs_path);
        amxc_var_clean(&data);
        amxc_var_clean(&ret);
    }

exit:
    return retval;
}

bool led_set_element(led_instance_t* led_info, cycle_element_t* cycle_info) {
    bool retval = led_set_fade(led_info, cycle_info->fade_interval_ms);
    retval &= led_set_color(led_info, cycle_info->color);
    retval &= led_set_brightness(led_info, cycle_info->brightness, false);
    led_info->current_element = &cycle_info->ll_it;
    return retval;
}

uint32_t led_brightness(const char* device_name, bool max) {
    uint32_t retval = 0;
    amxo_parser_t* parser = get_parser();
    const char* const so_name = GETP_CHAR(&parser->config, "so-name");
    const char* const mod_name = GETP_CHAR(&parser->config, "mod-name");
    amxc_var_t data;
    amxc_var_t ret;
    amxc_string_t sysfs_path;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_string_init(&sysfs_path, 0);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_string_setf(&sysfs_path, "%s", get_default_path());
    amxc_var_add_key(cstring_t, &data, "sysfs_path", amxc_string_get(&sysfs_path, 0));
    amxc_var_add_key(cstring_t, &data, "device_name", device_name);
    amxc_var_add_key(bool, &data, "get_max_brightness", max);

    when_failed_trace(amxm_execute_function(so_name, mod_name, "led-brightness", &data, &ret) != 0, exit, ERROR, "Call to led_brightness failed");

    retval = amxc_var_constcast(uint32_t, &ret);

exit:
    amxc_string_clean(&sysfs_path);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);

    return retval;
}
