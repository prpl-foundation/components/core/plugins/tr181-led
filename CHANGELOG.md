# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.8.3 - 2024-08-19(09:53:33 +0000)

### Fixes

- [tr181-led] Error on sysfs mode parameter at init of tr181-led

## Release v0.8.2 - 2024-07-20(06:12:09 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v0.8.1 - 2024-07-08(09:18:41 +0000)

### Other

- amx plugin should not run as root user

## Release v0.8.0 - 2024-07-02(08:54:37 +0000)

### New

- Export brightness management functions to external module

## Release v0.7.1 - 2024-04-10(08:47:36 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.7.0 - 2024-03-25(08:32:11 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.6.0 - 2024-03-19(14:40:20 +0000)

### New

- [tr181-led] Add reboot persistence in configuration

## Release v0.5.2 - 2024-03-11(16:45:50 +0000)

### Other

- [tr181-led] Add support for led button

## Release v0.5.1 - 2024-02-20(10:40:00 +0000)

### Fixes

- [tr181-led] Add upgrade persistence capability for X_SOFTATHOME-COM_Config

## Release v0.5.0 - 2024-02-08(14:11:51 +0000)

### Security

- tr181-led Set objects and parameters as protected

## Release v0.4.12 - 2024-01-23(10:39:58 +0000)

### Fixes

- tr181-led Failed to open /sys/class/leds/safran-mcu-ledX/delay_off

## Release v0.4.11 - 2024-01-18(13:31:29 +0000)

### Other

- [PCM] Add PCM support in tr181-led

## Release v0.4.10 - 2024-01-04(15:49:37 +0000)

### Fixes

- [tr181-led] Brightness not applied in the case of multiple LEDs

## Release v0.4.9 - 2023-12-06(13:41:41 +0000)

### Fixes

- [TR181 LED] Load defaults directly from odl file

## Release v0.4.8 - 2023-12-01(16:58:45 +0000)

### Fixes

- [TR181 LED] Update CyclePeriodRepetitions correctly

## Release v0.4.7 - 2023-11-30(12:35:34 +0000)

### Fixes

- Improve initialization + use transactions to update dm

## Release v0.4.6 - 2023-11-30(12:06:33 +0000)

### Fixes

- - [tr181-led] Add support for combining leds

## Release v0.4.5 - 2023-11-27(10:45:00 +0000)

### Fixes

- [TR181 LED] Enable sahtrace

## Release v0.4.4 - 2023-11-15(16:18:30 +0000)

### Fixes

- - [tr181-led] Do not call cleaning functions for parser and dm

## Release v0.4.3 - 2023-11-14(13:15:12 +0000)

### Changes

- - [tr181-led] Stop loop on CycleElements when Duration is not provided

## Release v0.4.2 - 2023-11-10(09:50:30 +0000)

### Other

- - [ACL] Admin cannot access to LEDs. missing config in webui.json

## Release v0.4.1 - 2023-11-09(08:54:51 +0000)

### Fixes

- - [ACL] Admin cannot access to LEDs. missing config in webui.json

## Release v0.4.0 - 2023-11-03(12:01:46 +0000)

### New

- - Support configuration of LED brightness

## Release v0.3.6 - 2023-10-13(13:48:55 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.3.5 - 2023-10-05(08:26:46 +0000)

### Other

- Opensource component

## Release v0.3.4 - 2023-09-15(06:57:10 +0000)

### Other

- - [ACL] Admin cannot access to LEDs.

## Release v0.3.3 - 2023-09-07(15:42:29 +0000)

### Other

- Update external URL

## Release v0.3.2 - 2023-08-04(11:25:44 +0000)

### Changes

- Move defaults to board-configurator

## Release v0.3.1 - 2023-08-02(11:42:38 +0000)

### Other

- Opensource component

## Release v0.3.0 - 2023-06-08(06:39:04 +0000)

### New

- [TR181-LED] Add init script

## Release v0.2.1 - 2023-04-05(11:06:37 +0000)

### Fixes

- [TR181-LED] plugin crashes when current cycle element is deleted

## Release v0.2.0 - 2023-03-07(10:01:05 +0000)

### New

-  [LED Manager] Use standard TR-181 datamodel to drive LED

## Release v0.1.0 - 2023-02-24(14:07:27 +0000)

### New

- [LED] Implement TR-181 LED standard datamodel

