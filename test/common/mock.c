/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>


#include "mock.h"

static struct event_base* base = NULL;
static int timeout = 0;

char* load_file(char const* path) {
    char* buffer = 0;
    long length;
    FILE* f = fopen(path, "rb");
    if(f) {
        fseek(f, 0, SEEK_END);
        length = ftell(f);
        fseek(f, 0, SEEK_SET);
        buffer = (char*) malloc((length + 1) * sizeof(char));
        if(buffer) {
            fread(buffer, sizeof(char), length, f);
        }
        fclose(f);
    }
    buffer[length] = '\0';

    return buffer;
}

static void el_signal_timers(UNUSED evutil_socket_t fd,
                             UNUSED short event,
                             UNUSED void* arg) {
    amxp_timers_calculate();
    amxp_timers_check();
    event_base_loopbreak(base);
}
static void test_timer_cb(UNUSED amxp_timer_t* t, void* priv) {
    int* timeout = (int*) priv;
    *timeout = 1;
    print_message("Test time out reached! (%s:%d)\n", __func__, __LINE__);
    event_base_loopbreak(base);
}


void wait_for_sigalarm(int seconds) {
    amxp_timer_t* test_timer = NULL;
    struct event* signal_alarm = NULL;

    timeout = 0;

    amxp_timer_new(&test_timer, test_timer_cb, &timeout);
    amxp_timer_start(test_timer, seconds * 1000);

    base = event_base_new();
    assert_non_null(base);

    signal_alarm = evsignal_new(base,
                                SIGALRM,
                                el_signal_timers,
                                NULL);
    event_add(signal_alarm, NULL);

    event_base_dispatch(base);

    amxp_timer_delete(&test_timer);
    event_del(signal_alarm);
    event_base_free(base);
    base = NULL;
    free(signal_alarm);
}

bool file_content_equals(char* file_name, char* content) {
    char* file_content = load_file(file_name);
    bool retval = (strcmp(file_content, content) == 0);
    free(file_content);
    return retval;
}

void init_led_files(void) {
    fclose(fopen(led_param(white, brightness), "w"));
    fclose(fopen(led_param(white, delay_on), "w"));
    fclose(fopen(led_param(white, delay_off), "w"));
    fclose(fopen(led_param(orange, brightness), "w"));
    fclose(fopen(led_param(orange, delay_on), "w"));
    fclose(fopen(led_param(white, delay_off), "w"));
    fclose(fopen(led_param(red, brightness), "w"));
    fclose(fopen(led_param(green, brightness), "w"));
    fclose(fopen(led_param(blue, brightness), "w"));
}
