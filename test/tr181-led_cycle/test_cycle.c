/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>


#include "dm_led.h"
#include "led.h"
#include "mock.h"
#include "test_cycle.h"

static amxd_dm_t dm;
static amxo_parser_t parser;


static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}
static void handle_alarm(int sig) {
    printf("sig %d\n", sig);
}


int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "cycle_element_changed", AMXO_FUNC(_cycle_element_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "cycle_element_added", AMXO_FUNC(_cycle_element_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "cycle_element_removed", AMXO_FUNC(_cycle_element_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "led_start_cycle", AMXO_FUNC(_led_start_cycle)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "led_stop_cycle", AMXO_FUNC(_led_stop_cycle)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "led_app_start", AMXO_FUNC(_led_app_start)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "get_prefix", AMXO_FUNC(get_prefix)), 0);



    assert_int_equal(amxo_parser_parse_file(&parser, LED_TEST_ODL, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, "test_cycle.odl", root_obj), 0);
    assert_int_equal(_led_main(0, &dm, &parser), 0);
    init_led_files();
    signal(SIGALRM, handle_alarm);
    _led_app_start(NULL, NULL, NULL);
    wait_for_sigalarm(1);
    handle_events();

    return 0;
}
int test_teardown(UNUSED void** state) {
    assert_int_equal(_led_main(AMXO_STOP, &dm, &parser), 0);
    amxo_resolver_import_close_all();
    amxm_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    return 0;
}

void test_cycle_update(UNUSED void** state) {
    led_instance_t* led_info = NULL;
    cycle_element_t* current_element = NULL;
    cycle_element_t* next_element = NULL;
    amxd_object_t* led_instance = amxd_dm_findf(&dm, "LEDs.LED.led_front.");
    assert_non_null(led_instance);
    led_info = led_instance->priv;
    assert_non_null(led_info);
    element_start_next(led_info->cycle_timer, led_instance);
    amxp_timer_stop(led_info->cycle_timer);
    assert_non_null(led_info->current_element);
    current_element = amxc_llist_it_get_data(led_info->current_element, cycle_element_t, ll_it);
    element_start_next(led_info->cycle_timer, led_instance);
    amxp_timer_stop(led_info->cycle_timer);
    next_element = amxc_llist_it_get_data(led_info->current_element, cycle_element_t, ll_it);
    assert_non_null(next_element);
    assert_int_not_equal(strcmp(current_element->obj_path, next_element->obj_path), 0);
    element_start_next(led_info->cycle_timer, led_instance);
    amxp_timer_stop(led_info->cycle_timer);
    next_element = amxc_llist_it_get_data(led_info->current_element, cycle_element_t, ll_it);
    assert_int_equal(strcmp(current_element->obj_path, next_element->obj_path), 0);
    element_start_next(led_info->cycle_timer, led_instance);
}
void test_period_start_stop(UNUSED void** state) {
    led_instance_t* led_info = NULL;
    amxd_object_t* led_instance = amxd_dm_findf(&dm, "LEDs.LED.1.");
    amxd_trans_t trans;

    assert_non_null(led_instance);
    led_info = led_instance->priv;
    assert_non_null(led_info);
    amxd_trans_init(&trans);
    assert_int_not_equal(amxp_timer_get_state(led_info->cycle_timer), amxp_timer_off);
    amxd_trans_select_pathf(&trans, "LEDs.LED.1.");
    assert_true(file_content_equals(led_param(orange, brightness), "255") || file_content_equals(led_param(white, brightness), "255"));
    amxd_trans_set_value(int32_t, &trans, "CyclePeriodRepetitions", 0);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    assert_int_equal(amxp_timer_get_state(led_info->cycle_timer), amxp_timer_off);
    assert_true(file_content_equals(led_param(orange, brightness), "0"));
    assert_true(file_content_equals(led_param(white, brightness), "0"));
    amxd_trans_clean(&trans);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LEDs.LED.1.");
    amxd_trans_set_value(int32_t, &trans, "CyclePeriodRepetitions", -1);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    wait_for_sigalarm(10);
    assert_int_not_equal(amxp_timer_get_state(led_info->cycle_timer), amxp_timer_off);
    assert_true(file_content_equals(led_param(orange, brightness), "255") || file_content_equals(led_param(white, brightness), "255"));
    amxd_trans_clean(&trans);
}

void test_cycle_change(UNUSED void** state) {
    amxd_object_t* led_instance = amxd_dm_findf(&dm, "LEDs.LED.led_front.");
    amxd_object_t* first_em = amxd_dm_findf(&dm, "LEDs.LED.1.CycleElement.1.");
    amxd_object_t* second_em = amxd_dm_findf(&dm, "LEDs.LED.1.CycleElement.2.");
    amxd_trans_t trans;
    cycle_element_t* em1_info = NULL;
    cycle_element_t* em2_info = NULL;
    rgb_t rgb_blue = {0, 0, 0};
    led_instance_t* led_info = led_instance->priv;
    rgb_blue.blue = 255;

    assert_non_null(first_em);
    assert_non_null(second_em);

    em1_info = first_em->priv;
    em2_info = second_em->priv;

    assert_non_null(em1_info);
    assert_non_null(em2_info);
    assert_int_equal(amxp_timer_stop(led_info->cycle_timer), 0);

    amxd_trans_init(&trans);
    assert_int_not_equal(em1_info->duration_ms, 4123);
    assert_int_not_equal(em1_info->fade_interval_ms, 2911);
    assert_true(em1_info->enabled);
    assert_false(same_color(em1_info->color, rgb_blue));
    assert_int_equal(amxc_llist_it_index_of(&em1_info->ll_it), 0);
    assert_int_equal(amxc_llist_it_index_of(&em2_info->ll_it), 1);

    amxd_trans_select_pathf(&trans, "LEDs.LED.1.CycleElement.1.");
    amxd_trans_set_value(uint32_t, &trans, "Duration", 4123);
    amxd_trans_set_value(uint32_t, &trans, "FadeInterval", 2911);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    amxd_trans_set_value(cstring_t, &trans, "Color", "0000FF");
    amxd_trans_set_value(int32_t, &trans, "X_PRPL-COM_Brightness", 53);
    amxd_trans_set_value(int32_t, &trans, "Order", 2);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    assert_int_equal(em1_info->duration_ms, 4123);
    assert_int_equal(em1_info->fade_interval_ms, 2911);
    assert_false(em1_info->enabled);
    assert_true(same_color(em1_info->color, rgb_blue));
    assert_int_equal(em1_info->brightness, 53);
    assert_int_equal(amxc_llist_it_index_of(&em1_info->ll_it), 1);
    assert_int_equal(amxc_llist_it_index_of(&em2_info->ll_it), 0);
    amxd_trans_clean(&trans);
}

void test_cycle_removal(UNUSED void** state) {
    assert_int_equal(0, 0);
    led_instance_t* led_info = NULL;
    cycle_element_t* cycle_info = NULL;
    amxd_object_t* led_instance = amxd_dm_findf(&dm, "LEDs.LED.1.");
    amxd_object_t* cycle_instance = amxd_dm_findf(&dm, "LEDs.LED.1.CycleElement.1.");
    amxd_trans_t trans;
    uint8_t len_elements = 0;
    uint8_t order_tail = 0;

    assert_non_null(led_instance);
    led_info = led_instance->priv;
    cycle_info = cycle_instance->priv;
    assert_non_null(led_info);
    assert_non_null(cycle_info);
    assert_int_equal(amxp_timer_stop(led_info->cycle_timer), 0);

    len_elements = amxc_llist_size(led_info->llist_cycle_period);
    assert_int_not_equal(len_elements, 0);
    order_tail = amxc_llist_it_index_of(&cycle_info->ll_it);
    assert_int_not_equal(order_tail, 0);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LEDs.LED.1.CycleElement.");
    amxd_trans_del_inst(&trans, 2, NULL);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    assert_int_equal(amxc_llist_size(led_info->llist_cycle_period), len_elements - 1);
    amxd_trans_clean(&trans);
}

void test_cycle_addition(UNUSED void** state) {
    assert_int_equal(0, 0);
    led_instance_t* led_info = NULL;
    cycle_element_t* cycle_info = NULL;
    amxd_object_t* led_instance = amxd_dm_findf(&dm, "LEDs.LED.1.");
    amxd_trans_t trans;
    uint8_t len_elements = 0;

    assert_non_null(led_instance);
    led_info = led_instance->priv;
    assert_non_null(led_info);
    assert_int_equal(amxp_timer_stop(led_info->cycle_timer), 0);

    len_elements = amxc_llist_size(led_info->llist_cycle_period);
    assert_int_not_equal(len_elements, 0);
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "LEDs.LED.1.CycleElement.");
    amxd_trans_add_inst(&trans, 0, "cycle_new_em");
    amxd_trans_set_value(uint32_t, &trans, "Order", 3);
    assert_int_equal(amxd_trans_apply(&trans, &dm), amxd_status_ok);
    handle_events();
    cycle_info = amxc_llist_it_get_data(led_info->llist_cycle_period->tail, cycle_element_t, ll_it);
    assert_non_null(cycle_info);
    assert_int_equal(amxc_llist_size(led_info->llist_cycle_period), len_elements + 1);
    amxd_trans_clean(&trans);
}
