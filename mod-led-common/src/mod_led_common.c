/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mod_led_common.h"

#define ME "mod-led-common"

static uint32_t mod_led_brightness(const char* device_name, bool max, const char* sysfs_path) {
    uint32_t retval = 0;
    char path[MAX_LEN_PATH] = {0};
    char buff[MAX_LEN_BUFFER] = {0};

    when_true_trace(device_name == NULL || sysfs_path == NULL, exit, ERROR, "Device name or sysfs path is NULL");

    snprintf(path, sizeof(path), "%s/%s/%s", sysfs_path, device_name, max == true ? SYSFS_LED_MAXBRIGHTNESS : SYSFS_LED_BRIGHTNESS);
    int fd = open(path, O_RDONLY);
    if((fd < 0) || (read(fd, buff, MAX_LEN_BUFFER) <= 0)) {
        SAH_TRACEZ_ERROR(ME, "Failed to read %s. Error message: %s", path, strerror(errno));
    } else if(sscanf(buff, "%u", &retval) <= 0) {
        retval = 1;
        SAH_TRACEZ_ERROR(ME, "Failed to get %s brightness information for [%s], setting value to %u.", max == true ? "max" : "", device_name, retval);
    }
    close(fd);

exit:
    return retval;
}

static int call_mod_led_brightness(UNUSED const char* function_name,
                                   amxc_var_t* args,
                                   UNUSED amxc_var_t* ret) {
    int res = -1;
    uint32_t rv = 0;
    const char* device_name = NULL;
    bool get_max_brightness = false;
    const char* sysfs_path = NULL;

    when_false_trace(amxc_var_type_of(args) == AMXC_VAR_ID_HTABLE, exit, ERROR, "Args not an htable");
    device_name = GET_CHAR(args, "device_name");
    get_max_brightness = GET_BOOL(args, "get_max_brightness");
    sysfs_path = GET_CHAR(args, "sysfs_path");

    rv = mod_led_brightness(device_name, get_max_brightness, sysfs_path);
    amxc_var_set(uint32_t, ret, rv);
    res = 0;
exit:
    return res;
}

static bool mod_led_sysfs_write(const char* led_name, const char* led_attribute_name, const char* led_attribute_value, const char* sysfs_path) {
    char path[MAX_LEN_PATH];

    if((led_name == NULL) || (led_attribute_name == NULL) || (led_attribute_value == NULL) || (sysfs_path == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Led parameters or sysfs path is NULL");
        return false;
    }

    snprintf(path, sizeof(path), "%s/%s/%s", sysfs_path, led_name, led_attribute_name);
    int fd = open(path, O_WRONLY | O_TRUNC);
    if(fd < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to open %s. Error message: %s", path, strerror(errno));
        return false;
    }
    if(write(fd, led_attribute_value, strlen(led_attribute_value)) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to write %s. Error message: %s", path, strerror(errno));
    }
    close(fd);

    return true;
}

static int call_mod_led_sysfs_write(UNUSED const char* function_name,
                                    amxc_var_t* args,
                                    amxc_var_t* ret) {
    int res = -1;
    bool rv = false;
    const char* led_name = NULL;
    const char* led_attribute_name = NULL;
    const char* led_attribute_value = NULL;
    const char* sysfs_path = NULL;

    when_false_trace(amxc_var_type_of(args) == AMXC_VAR_ID_HTABLE, exit, ERROR, "Args not an htable");
    led_name = GET_CHAR(args, "led_name");
    led_attribute_name = GET_CHAR(args, "led_attribute_name");
    led_attribute_value = GET_CHAR(args, "led_attribute_value");
    sysfs_path = GET_CHAR(args, "sysfs_path");

    rv = mod_led_sysfs_write(led_name, led_attribute_name, led_attribute_value, sysfs_path);

    amxc_var_set(bool, ret, rv);
    res = 0;
exit:
    return res;
}

static bool mod_led_sysfs_write_uint32(const char* led_name, const char* led_attribute_name, uint32_t led_attribute_value, const char* sysfs_path) {
    char buff[MAX_LEN_BUFFER] = {0};

    snprintf(buff, sizeof(buff), "%u", led_attribute_value);
    return mod_led_sysfs_write(led_name, led_attribute_name, buff, sysfs_path);
}

static bool mod_led_set_brightness(int32_t brightness_info,
                                   bool led_suspend,
                                   bool force,
                                   amxc_var_t* led_device,
                                   const char* sysfs_path) {
    bool retval = false;

    amxc_var_for_each(device_ht, led_device) {
        int32_t max_brightness = GET_INT32(device_ht, "max_brightness");
        const char* sysfs_name = GET_CHAR(device_ht, "sysfs_name");
        int32_t brightness_val = 0;

        if((max_brightness >= 0) && ((force == true) || (mod_led_brightness(sysfs_name, false, sysfs_path) > 0))) {
            if(led_suspend != true) {
                // led_apply_colors sets brightness to either 0 or max_brightness, so the brightness is computed from max_brightness
                brightness_val = (int32_t) floor(((double) brightness_info / 100) * (double) max_brightness);
            }
            retval &= mod_led_sysfs_write_uint32(sysfs_name, SYSFS_LED_BRIGHTNESS, brightness_val, sysfs_path);
        }
    }

    return retval;
}

static int call_mod_led_set_brightness(UNUSED const char* function_name,
                                       amxc_var_t* args,
                                       amxc_var_t* ret) {
    int res = -1;
    bool retval = false;
    int32_t brightness_info = -1;
    bool led_suspend = false;
    bool force = false;
    amxc_var_t* led_device = NULL;
    const char* sysfs_path = NULL;

    when_false_trace(amxc_var_type_of(args) == AMXC_VAR_ID_HTABLE, exit, ERROR, "Args not an htable");
    brightness_info = GET_INT32(args, "brightness_info");
    led_suspend = GET_BOOL(args, "led_suspend");
    force = GET_BOOL(args, "force");
    led_device = GET_ARG(args, "led_devices");
    sysfs_path = GET_CHAR(args, "sysfs_path");

    retval = mod_led_set_brightness(brightness_info, led_suspend, force, led_device, sysfs_path);

    amxc_var_set(bool, ret, retval);
    res = 0;

exit:
    return res;
}

static AMXM_CONSTRUCTOR common_module_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    SAH_TRACEZ_INFO(ME, "Starting common module");
    amxm_module_register(&mod, so, MOD_LED_DEFAULT_CTRL);
    amxm_module_add_function(mod, "led-sysfs-write", call_mod_led_sysfs_write);
    amxm_module_add_function(mod, "led-brightness", call_mod_led_brightness);
    amxm_module_add_function(mod, "led-set-brightness", call_mod_led_set_brightness);

    return 0;
}

static AMXM_DESTRUCTOR common_module_stop(void) {
    SAH_TRACEZ_INFO(ME, "Stopping common module");
    return 0;
}
