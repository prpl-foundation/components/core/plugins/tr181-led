/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__LED_H__)
#define __LED_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>
#include <errno.h>
#include <math.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "led"

#define LED_TR181_PATH "LEDs.LED"
#define LED_DEVICE_PATH "/sys/class/leds"
#define LED_DEFAULT_TRIGGER "none"

#define SYSFS_LED_BRIGHTNESS "brightness"
#define SYSFS_LED_DELAY_ON "delay_on"
#define SYSFS_LED_DELAY_OFF "delay_off"
#define SYSFS_LED_MODE "mode"
#define SYSFS_LED_TRIGGER "trigger"

#define MAX_LEN_PATH 256
#define MAX_LEN_BUFFER 32

#ifndef same_color
#define same_color(x, y) ((x.blue) == (y.blue) && (x.red) == (y.red) && (x.green) == (y.green))
#endif

#ifndef fail_when_null
#define fail_when_null(x) if((x) == NULL) {  return 1; }
#endif

typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} rgb_t;

typedef struct {
    const char* red_led;
    const char* green_led;
    const char* blue_led;
} rgb_led_t;

typedef struct {
    amxp_timer_t* cycle_timer;
    amxc_llist_t* llist_led_device;
    amxc_llist_t* llist_cycle_period;
    amxc_llist_it_t* current_element;
    bool controlled;
    bool suspend;
} led_instance_t;

typedef struct {
    amxc_llist_it_t ll_it;
    rgb_t color;
    int32_t max_brightness;
    const char* sysfs_name;
    bool has_fade;
} led_device_t;

typedef struct {
    rgb_t color;
    int32_t brightness;
    uint32_t duration_ms;
    uint32_t fade_interval_ms;
    bool enabled;
    char* obj_path;
    amxc_llist_it_t ll_it;
} cycle_element_t;

//cycle execution
void element_start_next(amxp_timer_t* timer, void* priv);
//Initialisation
void element_init_info(cycle_element_t* cycle_info, amxd_object_t* celement_instance);
int led_init_all(void);
//LED device access
uint32_t led_brightness(const char* device_name, bool max);
bool led_device_exists(const char* device_name);
bool led_validate_trigger(const char* device_name, const char* trigger, const char* parameter, const char* value);
int led_init(amxd_object_t* led_instance);
bool led_set_element(led_instance_t* led_info, cycle_element_t* cycle_info);
bool led_set_brightness(led_instance_t* led_info, int32_t brightness, bool force);
bool led_set_fade(led_instance_t* led_info, uint32_t fade_ms);
bool led_set_color(led_instance_t* led_info, rgb_t color_info);
bool led_set_mode(const char* device_name, const char* trigger, const char* mode);
//Cleaning used memory
int led_deinit_all(void);
void led_device_clean(amxc_llist_it_t* it);
void element_clean(amxc_llist_it_t* it);
//misc
void hex_to_rgb(const char* hex, rgb_t* rgb_representation);

int led_open_sysfsctrl(void);

#ifdef __cplusplus
}
#endif

#endif // __LED_H__
